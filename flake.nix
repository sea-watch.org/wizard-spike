{
  description = "A very basic flake";

  inputs = {
    flake-utils.url = "github:numtide/flake-utils";
  };

  outputs = { self, flake-utils, nixpkgs }:
    flake-utils.lib.eachDefaultSystem (system:
      let pkgs = nixpkgs.legacyPackages.${system};
      in {
        apps.devserver = {
          type = "app";
          program = "${pkgs.writeShellApplication {
            name = "devserver";
            runtimeInputs = [ pkgs.python3 ];
            text = ''
              exec python -m http.server 7777
            '';
          }}/bin/devserver";
        };
      }
    );
}
