class FormWizard extends HTMLFormElement {
  navButtonAttribute = "form-wizard_goto";
  pageAttribute = "form-wizard_page";
  currentPageClass = "form-wizard_current";
  nextButtonClass = "form-wizard_next";

  constructor() {
    super();

    this.setupPageNavigationHandlers();
    if (document.location.search) {
      const code = document.createElement("CODE");
      code.innerHTML = document.location.search;
      this.querySelector("nav").insertAdjacentElement("afterend", code);
    }
  }

  setupPageNavigationHandlers() {
    this.querySelectorAll(`[${this.navButtonAttribute}]`).forEach((e) => {
      e.addEventListener("click", this.onNavButtonClicked.bind(this));
    });
    this.addEventListener("submit", this.onSubmit);
  }

  /////////////
  // Navigation

  gotoPage(/** @type {string} */ page) {
    const nextPageElement = this.querySelector(
      `[${this.pageAttribute}="${page}"]`
    );
    if (nextPageElement == null) {
      throw `next page (${nextPage}) not found?!?`;
    }

    const nextNavElement = this.querySelector(
      `nav [${this.navButtonAttribute}="${page}"]`
    );
    if (nextNavElement == null) {
      throw `nav element for ${page} not found!`;
    }

    this.querySelectorAll(`.${this.currentPageClass}`).forEach((e) =>
      e.classList.remove(this.currentPageClass)
    );
    nextPageElement.classList.add(this.currentPageClass);
    nextNavElement.classList.add(this.currentPageClass);
  }

  /**
   * @returns {string}
   */
  currentPage() {
    const currentPageElement = this.querySelector(
      `.${this.currentPageClass}[${this.pageAttribute}]`
    );
    if (currentPageElement == null) throw `no current page found!?!`;
    return currentPageElement.getAttribute(this.pageAttribute);
  }

  /**
   * @returns {string[]}
   */
  pages() {
    return Array.from(
      this.querySelectorAll(`nav [${this.navButtonAttribute}]`)
    ).map((navButton) => navButton.getAttribute(this.navButtonAttribute));
  }

  /////////////////
  // Event handlers

  onNavButtonClicked(/** @type {MouseEvent} */ event) {
    // XXX: What if the current page is invalid?
    // XXX: What if e.g. we're going from page 1 to 3 without filling 2? Forbidden?
    // XXX: The real question here is, is it an invariant that previous pages are valid?
    const /** @type {HTMLElement} */ target = event.target;
    const nextPage = target.getAttribute(this.navButtonAttribute);
    if (nextPage == null) {
      throw `expected navButton to have ${this.navButtonAttribute} attribute`;
    }
    this.gotoPage(nextPage);
  }

  onSubmit(/** @type {SubmitEvent} */ event) {
    // XXX: What if a previous page has become invalid for some reason? Can that happen?

    // 1. Validate the current page
    const currentPageValid = this.validateCurrentPage();
    if (!currentPageValid) return;

    // 2. If we're on the last page, actually submit
    const pages = this.pages();
    const currentPage = this.currentPage();
    const currentPageIndex = pages.indexOf(currentPage);
    if (currentPageIndex == -1) {
      throw `current page (${currentPage}) wasn't found in navigation?!?`;
    }
    const nextPageIndex = currentPageIndex + 1;
    if (nextPageIndex == pages.length) {
      return; // let the form submit.
    }

    // 3. If not, move to the next page
    event.preventDefault();
    const nextPage = pages[currentPageIndex + 1];
    if (nextPage == null) {
      throw `next page (index ${currentPageIndex + 1}) wasn't found?!?`;
    }

    this.gotoPage(nextPage);
  }

  /////////////
  // Validation

  validateCurrentPage() {
    const pageElement = this.querySelector(
      `.${this.currentPageClass}[${this.pageAttribute}]`
    );
    if (pageElement == null) {
      throw `no current page found?!?`;
    }

    for (const control of this.elements) {
      if (pageElement.contains(control) && control.willValidate) {
        if (!control.reportValidity()) {
          return false;
        }
      }
    }
    return true;
  }
}
customElements.define("form-wizard", FormWizard, { extends: "form" });
